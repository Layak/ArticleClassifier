import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import asg.cliche.Command;
import asg.cliche.ShellFactory;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;


public class NaiveBayes {

    HashMap<String, HashMap<String, Long>> dictionary;
    HashMap<String, Long> classes;
    Set<String> vocabulary;

    public NaiveBayes() {
        dictionary = new HashMap<String, HashMap<String, Long>>();
        classes = new HashMap<String, Long>();
        vocabulary = new HashSet<String>();
    }

    private String readFile(String filePath) throws IOException {
        return FileUtils.readFileToString(new File(filePath));
    }

    private List<String> getTokens(String text) throws IOException {
        StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
        LinkedList<String> tokens = new LinkedList<String>();
        StringReader reader = new StringReader(text);
        reader.reset();
        TokenStream stream = analyzer.tokenStream("test", reader);
        stream.reset();
        while (stream.incrementToken()) {
            CharTermAttribute term =
              (CharTermAttribute) stream.getAttribute(CharTermAttribute.class);
            tokens.add(term.toString());
        }
        stream.end();
        stream.close();
        return tokens;
    }

    private double computePrior(String className) {
        /*
         * TODO: Compute the prior of a class (P(c)) for class "className"
         */
        return 0.0;
    }

    private double computeConditionalProb(String token, String className) {
        /*
         * TODO: Compute the conditional probabilities P(f/c)
         * with Laplace smoothing for class "className" using as
         * feature the token "token"
         */
        return 0.0;
    }

    @Command
    public void train(String filePath, String className) throws IOException {
        /*
         * TODO: Use the information in the file referenced by "filePath"
         * as training for the class "className"
         */
        System.out.println("You are calling train with args: "
                           + filePath + " " + className);
    }

    @Command
    public String classify(String filePath) throws IOException {
        /*
         * TODO: Return the name of the class that fits best
         * with the contents within the file referenced by "filePath"
         */
        return null;
    }

    @Command
    public void clear() {
        // Clear the data structures
        vocabulary.clear();
        dictionary.clear();
        classes.clear();
    }

    public static void main(String[] args) {

        try {
            // Create shell to run train and classify commands
            ShellFactory.createConsoleShell("$", "",
                                            new NaiveBayes()).commandLoop();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
